import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'
import * as v800 from '../v800'
import * as v802 from '../v802'

export const account =  {
    /**
     *  The full account information for a particular account ID.
     */
    v800: new StorageType('System.Account', 'Default', [v800.AccountId32], v800.AccountInfo) as AccountV800,
}

/**
 *  The full account information for a particular account ID.
 */
export interface AccountV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.AccountInfo
    get(block: Block, key: v800.AccountId32): Promise<(v800.AccountInfo | undefined)>
    getMany(block: Block, keys: v800.AccountId32[]): Promise<(v800.AccountInfo | undefined)[]>
    getKeys(block: Block): Promise<v800.AccountId32[]>
    getKeys(block: Block, key: v800.AccountId32): Promise<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<v800.AccountId32[]>
    getPairs(block: Block): Promise<[k: v800.AccountId32, v: (v800.AccountInfo | undefined)][]>
    getPairs(block: Block, key: v800.AccountId32): Promise<[k: v800.AccountId32, v: (v800.AccountInfo | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: v800.AccountId32, v: (v800.AccountInfo | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<[k: v800.AccountId32, v: (v800.AccountInfo | undefined)][]>
}

export const extrinsicCount =  {
    /**
     *  Total extrinsics count for the current block.
     */
    v800: new StorageType('System.ExtrinsicCount', 'Optional', [], sts.number()) as ExtrinsicCountV800,
}

/**
 *  Total extrinsics count for the current block.
 */
export interface ExtrinsicCountV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block): Promise<(number | undefined)>
}

export const blockWeight =  {
    /**
     *  The current weight for the block.
     */
    v800: new StorageType('System.BlockWeight', 'Default', [], v800.PerDispatchClass) as BlockWeightV800,
}

/**
 *  The current weight for the block.
 */
export interface BlockWeightV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.PerDispatchClass
    get(block: Block): Promise<(v800.PerDispatchClass | undefined)>
}

export const allExtrinsicsLen =  {
    /**
     *  Total length (in bytes) for all extrinsics put together, for the current block.
     */
    v800: new StorageType('System.AllExtrinsicsLen', 'Optional', [], sts.number()) as AllExtrinsicsLenV800,
}

/**
 *  Total length (in bytes) for all extrinsics put together, for the current block.
 */
export interface AllExtrinsicsLenV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block): Promise<(number | undefined)>
}

export const blockHash =  {
    /**
     *  Map of block numbers to block hashes.
     */
    v800: new StorageType('System.BlockHash', 'Default', [sts.number()], v800.H256) as BlockHashV800,
}

/**
 *  Map of block numbers to block hashes.
 */
export interface BlockHashV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.H256
    get(block: Block, key: number): Promise<(v800.H256 | undefined)>
    getMany(block: Block, keys: number[]): Promise<(v800.H256 | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (v800.H256 | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (v800.H256 | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (v800.H256 | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (v800.H256 | undefined)][]>
}

export const extrinsicData =  {
    /**
     *  Extrinsics data for the current block (maps an extrinsic's index to its data).
     */
    v800: new StorageType('System.ExtrinsicData', 'Default', [sts.number()], sts.bytes()) as ExtrinsicDataV800,
}

/**
 *  Extrinsics data for the current block (maps an extrinsic's index to its data).
 */
export interface ExtrinsicDataV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): Bytes
    get(block: Block, key: number): Promise<(Bytes | undefined)>
    getMany(block: Block, keys: number[]): Promise<(Bytes | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (Bytes | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (Bytes | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (Bytes | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (Bytes | undefined)][]>
}

export const number =  {
    /**
     *  The current block number being processed. Set by `execute_block`.
     */
    v800: new StorageType('System.Number', 'Default', [], sts.number()) as NumberV800,
}

/**
 *  The current block number being processed. Set by `execute_block`.
 */
export interface NumberV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number
    get(block: Block): Promise<(number | undefined)>
}

export const parentHash =  {
    /**
     *  Hash of the previous block.
     */
    v800: new StorageType('System.ParentHash', 'Default', [], v800.H256) as ParentHashV800,
}

/**
 *  Hash of the previous block.
 */
export interface ParentHashV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.H256
    get(block: Block): Promise<(v800.H256 | undefined)>
}

export const digest =  {
    /**
     *  Digest of the current block, also part of the block header.
     */
    v800: new StorageType('System.Digest', 'Default', [], v800.Digest) as DigestV800,
}

/**
 *  Digest of the current block, also part of the block header.
 */
export interface DigestV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.Digest
    get(block: Block): Promise<(v800.Digest | undefined)>
}

export const events =  {
    /**
     *  Events deposited for the current block.
     * 
     *  NOTE: The item is unbound and should therefore never be read on chain.
     *  It could otherwise inflate the PoV size of a block.
     * 
     *  Events have a large in-memory size. Box the events to not go out-of-memory
     *  just in case someone still reads them from within the runtime.
     */
    v800: new StorageType('System.Events', 'Default', [], sts.array(() => v800.EventRecord)) as EventsV800,
    /**
     *  Events deposited for the current block.
     * 
     *  NOTE: The item is unbound and should therefore never be read on chain.
     *  It could otherwise inflate the PoV size of a block.
     * 
     *  Events have a large in-memory size. Box the events to not go out-of-memory
     *  just in case someone still reads them from within the runtime.
     */
    v802: new StorageType('System.Events', 'Default', [], sts.array(() => v802.EventRecord)) as EventsV802,
}

/**
 *  Events deposited for the current block.
 * 
 *  NOTE: The item is unbound and should therefore never be read on chain.
 *  It could otherwise inflate the PoV size of a block.
 * 
 *  Events have a large in-memory size. Box the events to not go out-of-memory
 *  just in case someone still reads them from within the runtime.
 */
export interface EventsV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.EventRecord[]
    get(block: Block): Promise<(v800.EventRecord[] | undefined)>
}

/**
 *  Events deposited for the current block.
 * 
 *  NOTE: The item is unbound and should therefore never be read on chain.
 *  It could otherwise inflate the PoV size of a block.
 * 
 *  Events have a large in-memory size. Box the events to not go out-of-memory
 *  just in case someone still reads them from within the runtime.
 */
export interface EventsV802  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v802.EventRecord[]
    get(block: Block): Promise<(v802.EventRecord[] | undefined)>
}

export const eventCount =  {
    /**
     *  The number of events in the `Events<T>` list.
     */
    v800: new StorageType('System.EventCount', 'Default', [], sts.number()) as EventCountV800,
}

/**
 *  The number of events in the `Events<T>` list.
 */
export interface EventCountV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number
    get(block: Block): Promise<(number | undefined)>
}

export const eventTopics =  {
    /**
     *  Mapping between a topic (represented by T::Hash) and a vector of indexes
     *  of events in the `<Events<T>>` list.
     * 
     *  All topic vectors have deterministic storage locations depending on the topic. This
     *  allows light-clients to leverage the changes trie storage tracking mechanism and
     *  in case of changes fetch the list of events of interest.
     * 
     *  The value has the type `(BlockNumberFor<T>, EventIndex)` because if we used only just
     *  the `EventIndex` then in case if the topic has the same contents on the next block
     *  no notification will be triggered thus the event might be lost.
     */
    v800: new StorageType('System.EventTopics', 'Default', [v800.H256], sts.array(() => sts.tuple(() => [sts.number(), sts.number()]))) as EventTopicsV800,
}

/**
 *  Mapping between a topic (represented by T::Hash) and a vector of indexes
 *  of events in the `<Events<T>>` list.
 * 
 *  All topic vectors have deterministic storage locations depending on the topic. This
 *  allows light-clients to leverage the changes trie storage tracking mechanism and
 *  in case of changes fetch the list of events of interest.
 * 
 *  The value has the type `(BlockNumberFor<T>, EventIndex)` because if we used only just
 *  the `EventIndex` then in case if the topic has the same contents on the next block
 *  no notification will be triggered thus the event might be lost.
 */
export interface EventTopicsV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): [number, number][]
    get(block: Block, key: v800.H256): Promise<([number, number][] | undefined)>
    getMany(block: Block, keys: v800.H256[]): Promise<([number, number][] | undefined)[]>
    getKeys(block: Block): Promise<v800.H256[]>
    getKeys(block: Block, key: v800.H256): Promise<v800.H256[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<v800.H256[]>
    getKeysPaged(pageSize: number, block: Block, key: v800.H256): AsyncIterable<v800.H256[]>
    getPairs(block: Block): Promise<[k: v800.H256, v: ([number, number][] | undefined)][]>
    getPairs(block: Block, key: v800.H256): Promise<[k: v800.H256, v: ([number, number][] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: v800.H256, v: ([number, number][] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: v800.H256): AsyncIterable<[k: v800.H256, v: ([number, number][] | undefined)][]>
}

export const lastRuntimeUpgrade =  {
    /**
     *  Stores the `spec_version` and `spec_name` of when the last runtime upgrade happened.
     */
    v800: new StorageType('System.LastRuntimeUpgrade', 'Optional', [], v800.LastRuntimeUpgradeInfo) as LastRuntimeUpgradeV800,
}

/**
 *  Stores the `spec_version` and `spec_name` of when the last runtime upgrade happened.
 */
export interface LastRuntimeUpgradeV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block): Promise<(v800.LastRuntimeUpgradeInfo | undefined)>
}

export const upgradedToU32RefCount =  {
    /**
     *  True if we have upgraded so that `type RefCount` is `u32`. False (default) if not.
     */
    v800: new StorageType('System.UpgradedToU32RefCount', 'Default', [], sts.boolean()) as UpgradedToU32RefCountV800,
}

/**
 *  True if we have upgraded so that `type RefCount` is `u32`. False (default) if not.
 */
export interface UpgradedToU32RefCountV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): boolean
    get(block: Block): Promise<(boolean | undefined)>
}

export const upgradedToTripleRefCount =  {
    /**
     *  True if we have upgraded so that AccountInfo contains three types of `RefCount`. False
     *  (default) if not.
     */
    v800: new StorageType('System.UpgradedToTripleRefCount', 'Default', [], sts.boolean()) as UpgradedToTripleRefCountV800,
}

/**
 *  True if we have upgraded so that AccountInfo contains three types of `RefCount`. False
 *  (default) if not.
 */
export interface UpgradedToTripleRefCountV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): boolean
    get(block: Block): Promise<(boolean | undefined)>
}

export const executionPhase =  {
    /**
     *  The execution phase of the block.
     */
    v800: new StorageType('System.ExecutionPhase', 'Optional', [], v800.Phase) as ExecutionPhaseV800,
}

/**
 *  The execution phase of the block.
 */
export interface ExecutionPhaseV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block): Promise<(v800.Phase | undefined)>
}

export const authorizedUpgrade =  {
    /**
     *  `Some` if a code upgrade has been authorized.
     */
    v800: new StorageType('System.AuthorizedUpgrade', 'Optional', [], v800.CodeUpgradeAuthorization) as AuthorizedUpgradeV800,
}

/**
 *  `Some` if a code upgrade has been authorized.
 */
export interface AuthorizedUpgradeV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block): Promise<(v800.CodeUpgradeAuthorization | undefined)>
}

export const inherentsApplied =  {
    /**
     *  Whether all inherents have been applied.
     */
    v802: new StorageType('System.InherentsApplied', 'Default', [], sts.boolean()) as InherentsAppliedV802,
}

/**
 *  Whether all inherents have been applied.
 */
export interface InherentsAppliedV802  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): boolean
    get(block: Block): Promise<(boolean | undefined)>
}
