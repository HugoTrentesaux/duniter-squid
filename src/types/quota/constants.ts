import {sts, Block, Bytes, Option, Result, ConstantType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const refundAccount =  {
    /**
     *  Account used to refund fee
     */
    v800: new ConstantType(
        'Quota.RefundAccount',
        v800.AccountId32
    ),
}
