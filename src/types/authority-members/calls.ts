import {sts, Block, Bytes, Option, Result, CallType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const goOffline =  {
    name: 'AuthorityMembers.go_offline',
    /**
     * See [`Pallet::go_offline`].
     */
    v800: new CallType(
        'AuthorityMembers.go_offline',
        sts.unit()
    ),
}

export const goOnline =  {
    name: 'AuthorityMembers.go_online',
    /**
     * See [`Pallet::go_online`].
     */
    v800: new CallType(
        'AuthorityMembers.go_online',
        sts.unit()
    ),
}

export const setSessionKeys =  {
    name: 'AuthorityMembers.set_session_keys',
    /**
     * See [`Pallet::set_session_keys`].
     */
    v800: new CallType(
        'AuthorityMembers.set_session_keys',
        sts.struct({
            keys: v800.SessionKeys,
        })
    ),
}

export const removeMember =  {
    name: 'AuthorityMembers.remove_member',
    /**
     * See [`Pallet::remove_member`].
     */
    v800: new CallType(
        'AuthorityMembers.remove_member',
        sts.struct({
            memberId: sts.number(),
        })
    ),
}

export const removeMemberFromBlacklist =  {
    name: 'AuthorityMembers.remove_member_from_blacklist',
    /**
     * See [`Pallet::remove_member_from_blacklist`].
     */
    v800: new CallType(
        'AuthorityMembers.remove_member_from_blacklist',
        sts.struct({
            memberId: sts.number(),
        })
    ),
}
