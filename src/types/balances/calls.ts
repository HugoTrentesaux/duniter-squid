import {sts, Block, Bytes, Option, Result, CallType, RuntimeCtx} from '../support'
import * as v800 from '../v800'
import * as v802 from '../v802'

export const transferAllowDeath =  {
    name: 'Balances.transfer_allow_death',
    /**
     * See [`Pallet::transfer_allow_death`].
     */
    v800: new CallType(
        'Balances.transfer_allow_death',
        sts.struct({
            dest: v800.MultiAddress,
            value: sts.bigint(),
        })
    ),
}

export const forceTransfer =  {
    name: 'Balances.force_transfer',
    /**
     * See [`Pallet::force_transfer`].
     */
    v800: new CallType(
        'Balances.force_transfer',
        sts.struct({
            source: v800.MultiAddress,
            dest: v800.MultiAddress,
            value: sts.bigint(),
        })
    ),
}

export const transferKeepAlive =  {
    name: 'Balances.transfer_keep_alive',
    /**
     * See [`Pallet::transfer_keep_alive`].
     */
    v800: new CallType(
        'Balances.transfer_keep_alive',
        sts.struct({
            dest: v800.MultiAddress,
            value: sts.bigint(),
        })
    ),
}

export const transferAll =  {
    name: 'Balances.transfer_all',
    /**
     * See [`Pallet::transfer_all`].
     */
    v800: new CallType(
        'Balances.transfer_all',
        sts.struct({
            dest: v800.MultiAddress,
            keepAlive: sts.boolean(),
        })
    ),
}

export const forceUnreserve =  {
    name: 'Balances.force_unreserve',
    /**
     * See [`Pallet::force_unreserve`].
     */
    v800: new CallType(
        'Balances.force_unreserve',
        sts.struct({
            who: v800.MultiAddress,
            amount: sts.bigint(),
        })
    ),
}

export const forceSetBalance =  {
    name: 'Balances.force_set_balance',
    /**
     * See [`Pallet::force_set_balance`].
     */
    v800: new CallType(
        'Balances.force_set_balance',
        sts.struct({
            who: v800.MultiAddress,
            newFree: sts.bigint(),
        })
    ),
}

export const forceAdjustTotalIssuance =  {
    name: 'Balances.force_adjust_total_issuance',
    /**
     * Adjust the total issuance in a saturating way.
     * 
     * Can only be called by root and always needs a positive `delta`.
     * 
     * # Example
     */
    v802: new CallType(
        'Balances.force_adjust_total_issuance',
        sts.struct({
            direction: v802.AdjustmentDirection,
            delta: sts.bigint(),
        })
    ),
}

export const burn =  {
    name: 'Balances.burn',
    /**
     * Burn the specified liquid free balance from the origin account.
     * 
     * If the origin's account ends up below the existential deposit as a result
     * of the burn and `keep_alive` is false, the account will be reaped.
     * 
     * Unlike sending funds to a _burn_ address, which merely makes the funds inaccessible,
     * this `burn` operation will reduce total issuance by the amount _burned_.
     */
    v802: new CallType(
        'Balances.burn',
        sts.struct({
            value: sts.bigint(),
            keepAlive: sts.boolean(),
        })
    ),
}
