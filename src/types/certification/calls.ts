import {sts, Block, Bytes, Option, Result, CallType, RuntimeCtx} from '../support'

export const addCert =  {
    name: 'Certification.add_cert',
    /**
     * See [`Pallet::add_cert`].
     */
    v800: new CallType(
        'Certification.add_cert',
        sts.struct({
            receiver: sts.number(),
        })
    ),
}

export const renewCert =  {
    name: 'Certification.renew_cert',
    /**
     * See [`Pallet::renew_cert`].
     */
    v800: new CallType(
        'Certification.renew_cert',
        sts.struct({
            receiver: sts.number(),
        })
    ),
}

export const delCert =  {
    name: 'Certification.del_cert',
    /**
     * See [`Pallet::del_cert`].
     */
    v800: new CallType(
        'Certification.del_cert',
        sts.struct({
            issuer: sts.number(),
            receiver: sts.number(),
        })
    ),
}

export const removeAllCertsReceivedBy =  {
    name: 'Certification.remove_all_certs_received_by',
    /**
     * See [`Pallet::remove_all_certs_received_by`].
     */
    v800: new CallType(
        'Certification.remove_all_certs_received_by',
        sts.struct({
            idtyIndex: sts.number(),
        })
    ),
}
