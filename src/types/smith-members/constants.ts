import {sts, Block, Bytes, Option, Result, ConstantType, RuntimeCtx} from '../support'

export const maxByIssuer =  {
    /**
     *  Maximum number of active certifications by issuer
     */
    v800: new ConstantType(
        'SmithMembers.MaxByIssuer',
        sts.number()
    ),
}

export const minCertForMembership =  {
    /**
     *  Minimum number of certifications to become a Smith
     */
    v800: new ConstantType(
        'SmithMembers.MinCertForMembership',
        sts.number()
    ),
}

export const smithInactivityMaxDuration =  {
    /**
     *  Maximum duration of inactivity before a smith is removed
     */
    v800: new ConstantType(
        'SmithMembers.SmithInactivityMaxDuration',
        sts.number()
    ),
}
