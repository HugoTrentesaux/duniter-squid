import {sts, Block, Bytes, Option, Result, CallType, RuntimeCtx} from '../support'

export const unlinkIdentity =  {
    name: 'Account.unlink_identity',
    /**
     * See [`Pallet::unlink_identity`].
     */
    v800: new CallType(
        'Account.unlink_identity',
        sts.unit()
    ),
}
