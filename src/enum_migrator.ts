import { EnumMigrationInfo } from "./types_custom";
import path from 'path';
import fs from 'fs';
import { camelToSnakeCase } from "./utils";

const migrationName = "EnumsMigration";

/**
 * Class to manage enum migrations from the GraphQL schema.
 */
export class EnumMigrator {
    private templatePath = 'assets/sql/squidMigration.js';
    private sqlFolderPath = 'assets/sql/';
    private migrationsFolderPath = 'db/migrations/';
    private enumMigrations: Map<string, EnumMigrationInfo> = new Map();

    addEnumValues(enumName: string, values: string[]) {
        const existingMigration = this.enumMigrations.get(enumName);
        if (existingMigration) {
            existingMigration.values.push(...values);
        } else {
            this.enumMigrations.set(enumName, { values, tableColumnMappings: [] });
        }
    }

    addTableColumnMapping(enumName: string, tableName: string, columnName: string) {
        const migration = this.enumMigrations.get(enumName);
        if (migration) {
            migration.tableColumnMappings.push({ tableName, columnName });
        } else {
            this.enumMigrations.set(enumName, { values: [], tableColumnMappings: [{ tableName, columnName }] });
        }
    }

    generateMigrationFiles() {
        // Generate SQL files
        this.createSqlFiles(migrationName);

        // Copy the template file if doesn't exist and replace the placeholders
        const files = fs.readdirSync(this.migrationsFolderPath);
        const migrationExists = files.some(file => file.endsWith(`${migrationName}.js`));
        if (!migrationExists) {
            this.createJsMigrationFile(migrationName);
        }
    }

    private createSqlFiles(migrationName: string) {
        let upSqlContent = '';
        let downSqlContent = '';

        // Generate up and down SQL content
        this.enumMigrations.forEach((enumInfo, enumName) => {
            const enumNameSnake = camelToSnakeCase(enumName);
            upSqlContent += `CREATE TABLE ${enumNameSnake} (value TEXT PRIMARY KEY);\n`;
            enumInfo.values.forEach((value) => {
                upSqlContent += `INSERT INTO ${enumNameSnake} (value) VALUES ('${value}');\n`;
            });

            // Manage table column mappings
            enumInfo.tableColumnMappings.forEach(mapping => {
                const tableNameSnake = camelToSnakeCase(mapping.tableName);
                const columnNameSnake = camelToSnakeCase(mapping.columnName);
                upSqlContent += `ALTER TABLE ${tableNameSnake} DROP COLUMN IF EXISTS ${columnNameSnake};\n`;
                upSqlContent += `ALTER TABLE ${tableNameSnake} ADD COLUMN ${columnNameSnake} TEXT REFERENCES ${enumNameSnake}(value);\n`;
            });

            downSqlContent += `DROP TABLE IF EXISTS ${enumNameSnake};\n`;
        });

        // Write to files
        fs.writeFileSync(path.join(this.sqlFolderPath, `${migrationName}_up.sql`), upSqlContent);
        fs.writeFileSync(path.join(this.sqlFolderPath, `${migrationName}_down.sql`), downSqlContent);
    }

    private createJsMigrationFile(migrationName: string) {
        const timestamp = Date.now();
        const templateContent = fs.readFileSync(this.templatePath, 'utf8');
        const migrationContent = templateContent
            .replace(/_sqlReqName_/g, migrationName)
            .replace(/_timestamp_/g, timestamp.toString());
        fs.writeFileSync(path.join(this.migrationsFolderPath, `${timestamp}-${migrationName}.js`), migrationContent);

    }
}
