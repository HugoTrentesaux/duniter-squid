import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, ManyToOne as ManyToOne_, Index as Index_, IntColumn as IntColumn_} from "@subsquid/typeorm-store"
import {Smith} from "./smith.model"

/**
 * Smith certification
 */
@Entity_()
export class SmithCert {
    constructor(props?: Partial<SmithCert>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    @Index_()
    @ManyToOne_(() => Smith, {nullable: true})
    issuer!: Smith

    @Index_()
    @ManyToOne_(() => Smith, {nullable: true})
    receiver!: Smith

    @IntColumn_({nullable: false})
    createdOn!: number
}
