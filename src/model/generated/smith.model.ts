import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, IntColumn as IntColumn_, Index as Index_, OneToOne as OneToOne_, JoinColumn as JoinColumn_, OneToMany as OneToMany_} from "@subsquid/typeorm-store"
import {Identity} from "./identity.model"
import {SmithCert} from "./smithCert.model"
import {SmithStatus} from "./_smithStatus"
import {SmithEvent} from "./smithEvent.model"

@Entity_()
export class Smith {
    constructor(props?: Partial<Smith>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    /**
     * Identity index
     */
    @Index_({unique: true})
    @IntColumn_({nullable: false})
    index!: number

    @Index_({unique: true})
    @OneToOne_(() => Identity, {nullable: true})
    @JoinColumn_()
    identity!: Identity

    /**
     * Smith certifications issued
     */
    @OneToMany_(() => SmithCert, e => e.issuer)
    smithCertIssued!: SmithCert[]

    /**
     * Smith certifications received
     */
    @OneToMany_(() => SmithCert, e => e.receiver)
    smithCertReceived!: SmithCert[]

    /**
     * Smith status of the identity
     */
    @Column_("varchar", {length: 8, nullable: true})
    smithStatus!: SmithStatus | undefined | null

    /**
     * history of the smith changes events
     */
    @OneToMany_(() => SmithEvent, e => e.smith)
    smithHistory!: SmithEvent[]

    /**
     * Last status change
     */
    @IntColumn_({nullable: true})
    lastChanged!: number | undefined | null

    /**
     * Number of forged blocks
     */
    @IntColumn_({nullable: false})
    forged!: number

    /**
     * Last forged block
     */
    @IntColumn_({nullable: true})
    lastForged!: number | undefined | null
}
