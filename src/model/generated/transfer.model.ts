import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, IntColumn as IntColumn_, Index as Index_, DateTimeColumn as DateTimeColumn_, ManyToOne as ManyToOne_, BigIntColumn as BigIntColumn_} from "@subsquid/typeorm-store"
import {Account} from "./account.model"
import {Event} from "./event.model"
import {TxComment} from "./txComment.model"

/**
 * Transfers
 */
@Entity_()
export class Transfer {
    constructor(props?: Partial<Transfer>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    /**
     * Block number of the transfer
     */
    @Index_()
    @IntColumn_({nullable: false})
    blockNumber!: number

    /**
     * Timestamp of the transfer (duplicate of block timestamp)
     */
    @Index_()
    @DateTimeColumn_({nullable: false})
    timestamp!: Date

    /**
     * Transfer issuer
     */
    @Index_()
    @ManyToOne_(() => Account, {nullable: true})
    from!: Account

    /**
     * Transfer receiver
     */
    @Index_()
    @ManyToOne_(() => Account, {nullable: true})
    to!: Account

    /**
     * Integer amount of the transfer
     */
    @Index_()
    @BigIntColumn_({nullable: false})
    amount!: bigint

    /**
     * Event the transfer was created in
     */
    @Index_()
    @ManyToOne_(() => Event, {nullable: true})
    event!: Event

    /**
     * Optional comment linked to the transfer
     */
    @Index_()
    @ManyToOne_(() => TxComment, {nullable: true})
    comment!: TxComment | undefined | null
}
