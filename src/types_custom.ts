import { StoreWithCache } from "@belopash/typeorm-store";
import {
  Account,
  Cert,
  CertEvent,
  ChangeOwnerKey,
  Identity,
  IdentityStatus,
  MembershipEvent,
  Smith,
  SmithEvent,
  SmithCert,
  PopulationHistory,
  Transfer,
  TxComment,
  UdReeval,
  UniversalDividend,
  Validator,
} from "./model";
import { Call, Event, Extrinsic, ProcessorContext } from "./processor";
import { AccountId32, MembershipRemovalReason, RemovalReason, RevocationReason } from "./types/v800";

// type aliases
export type BlockNumber = number;
export type Address = string;
export type B58Pubkey = string;
export type IdtyIndex = number;
export type Ctx = ProcessorContext<StoreWithCache>;

// =========================== Genesis =========================== //

// define genesis interfaces
// duniter build-spec --chain gdev_dev 1> ./specs.json
// pallet / genesis config
export interface Genesis {
  system: any;
  account: GenAccounts;
  babe: any;
  parameters: any;
  balances: any;
  authorityMembers: GenInitialAuthorities;
  session: any;
  grandpa: any;
  imOnline: any;
  authorityDiscovery: any;
  sudo: any;
  technicalCommittee: any;
  universalDividend: any;
  identity: GenIdentities;
  membership: GenMemberships;
  certification: GenCerts;
  smithMembers: GenSmithMembers;
  treasury: any;
}

// file exported by py-g1-migrator used as input for duniter
// this file is only used for current_block.number at the moment
export interface Genv1 {
  first_ud_value: number
  first_ud_reeval: number
  current_block: {
    number: number
    medianTime: number
  }
  initial_monetary_mass: number
  identities: undefined
  wallets: undefined
}

// v1 transaction history
export type TransactionHistory = Array<Tx>;
export interface Tx {
  blockNumber: BlockNumber
  timestamp: number
  from: B58Pubkey
  to: B58Pubkey
  amount: number
  comment: string
}

// warning: these genesis types are mostly descriptive and not stictly relevant
// example: Maps are actually JSON objects 

interface GenIdentities {
  identities: Array<GenIdentity>;
}
interface GenIdentity {
  index: number;
  name: Array<number>;
  value: GenIdtyValue;
}
interface GenIdtyValue {
  data: GenIdtyData;
  next_creatable_identity_on: number;
  old_owner_key: string | null;
  owner_key: string;
  removable_on: number;
  status: IdentityStatus;
}
interface GenIdtyData {
  first_eligible_ud: number;
}

interface GenMemberships {
  memberships: Map<IdtyIndex, GenMembership>;
}
interface GenMembership {
  expire_on: number;
}

interface GenSmithMembers {
  initialSmiths: Map<string, [boolean, number[]]>;
}

interface GenAccounts {
  accounts: Map<string, GenAccount>;
  treasuryBalance: number;
}
interface GenAccount {
  random_id: string;
  balance: number;
  is_identity: boolean;
}

interface GenCerts {
  applyCertPeriodAtGenesis: boolean;
  certsByReceiver: Map<number, Map<number, number>>;
}

interface GenInitialAuthorities {
  initialAuthorities: Map<number, Array<any>>;
}

export interface Certification {
  issuer: string;
  receiver: string;
}

// v1 blocks number will be converted to negative height
export interface BlockV1 {
  height: BlockNumber;
  timestamp: number;
  hash: string;
  parentHash: string | null;
  validator: AccountId32;
  version: number;
  hasEvent: boolean;
}
export interface Certv1 {
  blockNumber: BlockNumber;
  issuer: IdtyIndex;
  receiver: IdtyIndex;
  type: string;
}

// =========================== DataHandler =========================== //

// a way to group data prepared for database insertion
export interface Data {
  accounts: Map<Address, Account>;
  identities: Map<IdtyIndex, Identity>;
  validators: Map<Address, Validator>;
  populationHistories: PopulationHistory[];
  smiths: Map<IdtyIndex, Smith>;
  membershipEvents: MembershipEvent[];
  smithEvents: SmithEvent[];
  changeOwnerKey: ChangeOwnerKey[];
  transfers: Map<string, Transfer>;
  certification: Map<[IdtyIndex, IdtyIndex], Cert>;
  certEvent: CertEvent[];
  smithCert: Map<[IdtyIndex, IdtyIndex], SmithCert>;
  universalDividend: UniversalDividend[];
  udReeval: UdReeval[];
  comments: TxComment[];
}

// =========================== Events =========================== //

// a way to group data returned from events
// this contains partial data to be turned into types
export interface NewData {
  accounts: NewAccountEvent[];
  killedAccounts: Address[];
  validators: BlockValidator[];
  populationHistories: AccPopulationHistory[];
  identitiesCreated: IdtyCreatedEvent[];
  identitiesConfirmed: IdtyConfirmedEvent[];
  identitiesRemoved: IdtyRemovedEvent[];
  identitiesRevoked: IdtyRevokedEvent[];
  idtyChangedOwnerKey: IdtyChangedOwnerKeyEvent[];
  membershipAdded: MembershipAddedEvent[];
  membershipRemoved: MembershipRemovedEvent[];
  membershipRenewed: MembershipRenewedEvent[];
  transfers: TransferEvent[];
  certCreation: CertCreationEvent[];
  certRenewal: CertRenewalEvent[];
  certRemoval: CertRemovalEvent[];
  accountLink: AccountLinkEvent[];
  accountUnlink: AccountUnlinkEvent[];
  smithCertAdded: SmithCertAddedEvent[];
  smithCertRemoved: SmithCertRemovedEvent[],
  smithPromoted: SmithPromotedEvent[];
  smithExcluded: SmithExcludedEvent[];
  smithInvited: SmithInvitedEvent[];
  smithAccepted: SmithAcceptedEvent[];
  universalDividend: UniversalDividendEvent[];
  udReeval: UdReevalEvent[];
  comments: CommentEvents[];
}

export interface AccPopulationHistory {
  activeAccountCount: number;
  memberCount: number;
  smithCount: number;
  blockNumber: BlockNumber;
}

// id is always the id of the creation event. Only present if event is absent
interface UniversalDividendEvent {
  blockNumber: BlockNumber;
  timestamp: Date;
  amount: bigint;
  monetaryMass: bigint;
  membersCount: bigint;
  event: Event;
}

interface UdReevalEvent {
  blockNumber: BlockNumber;
  timestamp: Date;
  newUdAmount: bigint;
  monetaryMass: bigint;
  membersCount: bigint;
  event: Event;
}

interface NewAccountEvent {
  blockNumber: BlockNumber;
  address: Address
}

interface TransferEvent {
  id: string;
  blockNumber: BlockNumber;
  timestamp: Date;
  from: Address;
  to: Address;
  amount: bigint;
  event: Event;
}

interface CommentEvents {
  blockNumber: BlockNumber
  event: Event
  hash: string
  sender: Address
}

interface CertCreationEvent {
  issuerId: IdtyIndex;
  receiverId: IdtyIndex;
  expireOn: BlockNumber;
  blockNumber: BlockNumber;
  event: Event;
}

interface CertRenewalEvent {
  issuerId: IdtyIndex;
  receiverId: IdtyIndex;
  expireOn: BlockNumber;
  blockNumber: BlockNumber;
  event: Event;
}

interface CertRemovalEvent {
  issuerId: IdtyIndex;
  receiverId: IdtyIndex;
  blockNumber: BlockNumber;
  event: Event;
}

interface BlockValidator {
  block: BlockNumber;
  validatorId: Address;
}

interface SmithPromotedEvent {
  id: string;
  idtyIndex: IdtyIndex;
  event: Event;
}

interface SmithCertAddedEvent {
  id: string;
  issuerId: IdtyIndex;
  receiverId: IdtyIndex;
  createdOn: BlockNumber;
  event: Event;
}

interface SmithCertRemovedEvent {
  id: string;
  issuerId: IdtyIndex;
  receiverId: IdtyIndex;
  event: Event;
}

interface SmithExcludedEvent {
  id: string;
  idtyIndex: IdtyIndex;
  event: Event;
}

interface SmithInvitedEvent {
  id: string;
  idtyIndex: IdtyIndex;
  invitedBy: IdtyIndex;
  event: Event;
}

interface SmithAcceptedEvent {
  id: string;
  idtyIndex: IdtyIndex;
  event: Event;
}

interface IdtyCreatedEvent {
  index: IdtyIndex;
  accountId: Address;
  blockNumber: BlockNumber;
  expireOn: BlockNumber;
  event: Event;
}

interface IdtyConfirmedEvent {
  id: string;
  index: IdtyIndex;
  name: string;
  blockNumber: BlockNumber;
  expireOn: BlockNumber;
  event: Event;
}

// note: redundant with membership event from indexer point of view
// interface IdtyValidatedEvent {
//   id: string;
//   index: IdtyIndex;
//   blockNumber: BlockNumber;
//   expireOn: BlockNumber;
//   event: Event;
// }

interface IdtyRemovedEvent {
  index: IdtyIndex;
  reason: RemovalReason;
}

interface IdtyRevokedEvent {
  id: string;
  index: IdtyIndex;
  reason: RevocationReason;
  blockNumber: BlockNumber;
  expireOn: BlockNumber;
  event: Event;
}

interface IdtyChangedOwnerKeyEvent {
  id: string;
  index: IdtyIndex;
  accountId: Address;
  blockNumber: BlockNumber;
  event: Event;
}

interface MembershipAddedEvent {
  index: IdtyIndex;
  expireOn: BlockNumber;
  event: Event;
}

interface MembershipRemovedEvent {
  index: IdtyIndex;
  reason: MembershipRemovalReason;
  expireOn: BlockNumber;
  event: Event;
}

interface MembershipRenewedEvent {
  index: IdtyIndex;
  expireOn: BlockNumber;
  event: Event;
}

interface AccountLinkEvent {
  accountId: Address;
  index: IdtyIndex;
  event: Event;
}

interface AccountUnlinkEvent {
  accountId: Address;
  event: Event;
}

// =========================== Hasura metadata script =========================== //

export interface ObjectRelationship {
  name: string;
  using: {
    foreign_key_constraint_on: string;
  };
}

export interface ArrayRelationship {
  name: string;
  using: {
    foreign_key_constraint_on: {
      column: string;
      table: {
        name: string;
        schema: string;
      };
    };
  };
}

export interface LookupRelationship {
  name: string;
  using: {
    foreign_key_constraint_on: {
      column: string;
      table: {
        name: string;
        schema: string;
      };
    };
  };
}

export interface ComputedField {
  name: string;
  definition: {
    function: {
      name: string;
      schema: string;
    };
  };
  comment?: string;
}

export interface HasuraTable {
  table: {
    name: string;
    schema: string;
  };
  object_relationships: (ObjectRelationship | LookupRelationship)[];
  array_relationships: ArrayRelationship[];
  computed_fields?: ComputedField[];
  select_permissions: {
    role: string;
    permission: {
      columns: string;
      filter: {};
      allow_aggregations: boolean;
    };
  }[];
}

export interface EntityProperties {
  [propertyName: string]: {
    type: {
      name: string;
      kind: string;
      entity: string;
      field: string;
    };
  };
}

export interface EnumMigrationInfo {
  values: string[];
  tableColumnMappings: EnumColumnMapping[];
}

export interface EnumColumnMapping {
  tableName: string;
  columnName: string;
}
